from django.test import TestCase, Client
from django.contrib.auth.models import auth, User

# Create your tests here.

class Testing(TestCase):
    
    def test_url_login(self):
        response = Client().get("/otentikasi/login/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "base.html")

    def test_url_signup(self):
        response = Client().get("/otentikasi/signup/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "base.html")

	#TEST VIEW
    def test_view_signup_signup_success(self):
        response = self.client.post('/otentikasi/signup/', data= {
                'username':'username',
                'email':'email@email.com',
                'password':'password123',
                'confirmPass':'password123'
                })
        self.assertEquals(response.status_code, 302)

    def test_view_login_login_success(self):
        User.objects.create_user(username='tester', password="password123")
        response = self.client.post('/otentikasi/login/?nextPage=/',data= {
                'username': 'tester',
                'password': 'password123'
                })
        self.assertEquals(response.status_code,302)

    def test_view_logout(self):
        response = Client().get("/otentikasi/logout")
        self.assertEqual(response.status_code, 301)