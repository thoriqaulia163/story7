from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import auth, User

# Create your views here.
def login_view(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(request, username=username, password=password)
        if user is not None:
            auth.login(request, user)
            nextPage = request.GET.get('nextPage', False)
            return redirect(nextPage)
            # Redirect to a success page.
            
    nextPage = request.GET.get('nextPage', False)     
    return render(request, 'otentikasi/login.html',{'next': nextPage})

def logout_view(request):
    logout(request)
    nextPage = request.GET.get('nextPage', False)
    return redirect(nextPage)
    # Redirect to a success page.

def signup_view(request):
    nextPage = request.GET.get('nextPage', False)
    response = {'pesan': '',
                'next':nextPage}
    if request.method == "POST":
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        confirm_password = request.POST['confirmPass']

        alluser = User.objects.all()
        for users in alluser:
            if username == users.username:
                return render(request,'otentikasi/signup.html', {'pesan':'username has used','next':nextPage})

        if password == confirm_password:
            user = User.objects.create_user(username=username, email=email, password = password)
            user.save()

            user = auth.authenticate(username=username, password=password)
            auth.login(request, user)
            return redirect('/')

        else:
            response = {'pesan': 'password confirm unmatch','next':nextPage}
    return render(request, 'otentikasi/signup.html', response)