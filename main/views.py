from django.shortcuts import render
from django.shortcuts import redirect


def home(request):
    response = {'next':'/'}
    return render(request, 'main/home.html',response)
