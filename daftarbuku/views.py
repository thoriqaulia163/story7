from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
import requests
import json

# Create your views here.

def daftarbuku(request):
    response = {'next':'/daftarbuku/'}
    return render(request, 'daftarbuku.html',response)
    
@login_required()
def cari_buku(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET.get('q',"")
    json_data = requests.get(url).content
    data = json.loads(json_data)
    return JsonResponse(data, safe=False)