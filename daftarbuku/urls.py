from django.urls import path

from . import views

app_name = 'daftarbuku'

urlpatterns = [
    path('', views.daftarbuku, name='daftarbuku'),
    path('caribuku/', views.cari_buku, name='caribuku'),
]
