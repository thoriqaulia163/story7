from django.test import TestCase, Client
from django.urls import reverse
from .views import daftarbuku, cari_buku
import json
import requests

# Create your tests here.

class Tester(TestCase):
	
	# TEST URL
	def test_url_daftarbuku(self):
		response = Client().get("/daftarbuku/");
		self.assertEquals(response.status_code, 200)

	def test_url_caribuku(self):
		response = self.client.get("/daftarbuku/caribuku/")
		self.assertEquals(response.status_code, 302)


	# TEST VIEW
	def test_view_daftarbuku(self):
		response = Client().get("/daftarbuku/")
		self.assertTemplateUsed(response, "base.html")
		self.assertTemplateUsed(response, "daftarbuku.html")
		self.assertEquals(response.status_code, 200)
        

	def test_view_caribuku_noLogin(self):
		response = self.client.get("/daftarbuku/caribuku/")
		self.assertEquals(response.status_code, 302)

              
