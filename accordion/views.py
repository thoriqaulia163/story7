from django.shortcuts import render
from django.contrib.auth.decorators import login_required

@login_required(login_url='/otentikasi/login/?nextPage=/daftarbuku/')
def accordion(request):
    response = {'next': '/accordion/'}
    return render(request, 'main/accordion.html', response)
